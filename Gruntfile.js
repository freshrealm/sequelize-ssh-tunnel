"use strict";
module.exports = function(grunt) {
    var mochaTestReporter = process.env.NODE_ENV === 'test' ? 'mocha-junit-reporter' : 'spec';
    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        notify: {
            tests: {
                options: {
                   message: 'Tests passed!'
                }
            }
        },
        watch: {
            options: {
                maxListeners: 20
            },
            tests: {
                files: ['**/*.js', '!node_modules/**'],
                tasks: ['mochaTest', 'jshint', 'notify:tests'],
                options: {
                    spawn: false,
                    atBegin: true
                },
            }
        },
        jshint: {
            options: {
                reporter: require('jshint-stylish'),
                jshintrc: true
            },
            all: ['**/*.js', '!node_modules/**']
        },
        mochaTest: {
            options: {
                reporter: mochaTestReporter,
                clearRequireCache: true
            },
            src: ['**/*.spec.js', '!node_modules/**']
        }
    });

    // Load the plugins.
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-notify');
    grunt.loadNpmTasks('grunt-mocha-test');

    // Tasks
    grunt.registerTask('tests', ['watch:tests']);
};
