# FreshRealm Sequelize SSH Tunnel Service

## Installation instructions
```
npm install https://bitbucket.org/freshrealm/sequelize-ssh-tunnel.git
```

___
```
new SequelizeTunnelService(dbConfig, tunnelConfig);
```
### Example usage
```
// basic connection to a database
var dbConfig = {
    database: 'dbName',
    username: 'root',
    password: '',
};
// with host and pattern to match model files
var dbConfig = {
    database: 'dbName',
    username: 'root',
    password: '',
    host: 'localhost',
    modelPattern: './**/*.model.js'
};
// ssh tunnel configuration
var tunnelConfig = {
    username: 'username',
    host: 'host',
    port: 22,
    privateKey: 'privateKey'
};
// initialize service
var sequelizeTunnelService = new SequelizeTunnelService(dbConfig, tunnelConfig);
```
### Params
| Name | Type | Description |
|---|---|---|
| dbConfig.database | String | The name of the database. |
| [dbConfig.username=null] | String | The username which is used to authenticate against the database. |
| [dbConfig.password=null] | String | The password which is used to authenticate against the database. |
| [dbConfig.modelPattern] | String | The pattern that will match filename of sequelize model files in your project. |
| [dbConfig] |  | Extra options that are accepted are found in the [Sequelize documentation](http://sequelize.readthedocs.io/en/latest/api/sequelize/) in the options object. |
| [tunnelConfig.username] | String | The username for the ssh tunnel connection. |
| [tunnelConfig.host] | String | The host for the ssh tunnel connection. |
| [tunnelConfig.port] | String | The port to connect to for the ssh tunnel connection. |
| [tunnelConfig.privateKey] | String | The key used to connect to the ssh tunnel. |
| [tunnelConfig.password] | String | The password used to connect to the ssh tunnel. |
| [tunnelConfig] |  | Extra options that are accepted are found in the [ssh2 documentation](https://github.com/mscdex/ssh2#api) |

---
```
getConnection(): <Promise>
```
Resolves with the connected Sequelize object. This is a regular Sequelize object and all functionality matches regular Sequelize patterns. If a model pattern was defined in dbConfig, this will also open all models matching that pattern in your project.
### Example Usage
```
sequelizeTunnelService.getConnection().then((db) => {
    return db.sequelize.query(`...`);
});
```

### Resolves
db.Sequelize = Sequelize library object used by the service

db.sequelize = connected database object

db[modelName] = sequelize model loaded from modelPattern
