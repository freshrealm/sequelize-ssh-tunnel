"use strict";
const Bluebird = require('bluebird');
const chai = require('chai');
const sinon = require('sinon');
const _ = require('lodash');
require('sinon-as-promised')(Bluebird);
const sinonChai = require('sinon-chai');
const should = chai.should();
chai.use(sinonChai);

const proxyquire = require('proxyquire');
const glob = require('glob');

let SequelizeTunnelService;

describe('sequelize-ssh-tunnel', function() {
    let sandbox;

    let sequelizeTunnelService;

    let tunnelMock;
    let getPortMock;
    let SequelizeMock;
    let globSyncStub;

    let dbConfig;
    let tunnelConfig;
    let modelPattern;

    const defaultDbConfig = {
        'database': 'database',
        'username': 'username',
        'password': 'password',
        'pool': 'pool',
        'host': 'host'
    };
    const defaultTunnelConfig = {
        username: 'username',
        host: 'host',
        port: 'port',
        privateKey: 'privateKey',
    };

    beforeEach(function() {
        sandbox = sinon.sandbox.create();

        tunnelMock = sandbox.stub();
        getPortMock = sandbox.stub();
        SequelizeMock = sandbox.stub();
        globSyncStub = sandbox.stub(glob, "sync");

        dbConfig = _.clone(defaultDbConfig);
        tunnelConfig = _.clone(defaultTunnelConfig);
        modelPattern = 'model-pattern';

        SequelizeTunnelService = proxyquire('./sequelize-ssh-tunnel.service', {
            'tunnel-ssh': tunnelMock,
            'get-port': getPortMock,
            'sequelize': SequelizeMock
        });

        sequelizeTunnelService = new SequelizeTunnelService(dbConfig, tunnelConfig);
    });

    afterEach(function() {
        sandbox.restore();
    });

    it('should exist', function() {
        should.exist(SequelizeTunnelService);
        should.exist(sequelizeTunnelService);
    });

    it('should bind the dbConfig and tunnelConfig from input', function() {
        sequelizeTunnelService.dbConfig.should.deep.equal(dbConfig);
        sequelizeTunnelService.tunnelConfig.should.deep.equal(tunnelConfig);
    });

    describe('#constructor', function() {
        beforeEach(function() {

        });

        it('should validate dbConfig', function() {
            should.throw(() => {
                new SequelizeTunnelService(null, tunnelConfig);
            });

            should.throw(() => {
                new SequelizeTunnelService({}, tunnelConfig);
            });

            should.throw(() => {
                new SequelizeTunnelService({
                    username: 'username',
                    password: 'password',
                }, tunnelConfig);
            });

            should.not.throw(() => {
                let defaultUser = new SequelizeTunnelService({
                    database: 'database',
                    password: 'password',
                }, tunnelConfig);
                should.equal(defaultUser.dbConfig.username, null);
            });

            should.not.throw(() => {
                let defaultPass = new SequelizeTunnelService({
                    database: 'database',
                    username: 'username',
                }, tunnelConfig);
                should.equal(defaultPass.dbConfig.password, null);
            });

            should.not.throw(() => {
                new SequelizeTunnelService({
                    database: 'database',
                    username: 'username',
                    password: 'password',
                }, tunnelConfig);
            });

            should.not.throw(() => {
                new SequelizeTunnelService({
                    database: 'database',
                    username: '',
                    password: 'password',
                }, tunnelConfig);
            });

            should.not.throw(() => {
                new SequelizeTunnelService({
                    database: 'database',
                    username: 'username',
                    password: null,
                }, tunnelConfig);
            });
        });

        it('should validate the structure of the optional replication field within dbConfig when provided', function() {
            should.throw(() => {
                new SequelizeTunnelService({
                    database: 'database',
                    username: 'username',
                    password: 'password',
                    replication: 'replication'
                }, tunnelConfig);
            });

            should.throw(() => {
                new SequelizeTunnelService({
                    database: 'database',
                    username: 'username',
                    password: 'password',
                    replication: {}
                }, tunnelConfig);
            });

            should.throw(() => {
                new SequelizeTunnelService({
                    database: 'database',
                    username: 'username',
                    password: 'password',
                    replication: {
                        write: {
                            host: 'host',
                            username: 'username',
                            password: 'password'
                        },
                        read: {
                            username: 'username',
                            password: 'password'
                        }
                    }
                }, tunnelConfig);
            });

            should.throw(() => {
                new SequelizeTunnelService({
                    database: 'database',
                    username: 'username',
                    password: 'password',
                    replication: {
                        write: {
                            host: 'host',
                            username: 'username',
                            password: 'password'
                        },
                        read: {
                            host: 'host',
                            password: 'password'
                        }
                    }
                }, tunnelConfig);
            });

            should.not.throw(() => {
                new SequelizeTunnelService({
                    database: 'database',
                    username: 'username',
                    password: 'password',
                    replication: {
                        write: {
                            host: 'host',
                            username: 'username',
                            password: 'password'
                        },
                        read: {
                            host: 'host',
                            username: 'username',
                        }
                    }
                }, tunnelConfig);
            });

            should.throw(() => {
                new SequelizeTunnelService({
                    database: 'database',
                    username: 'username',
                    password: 'password',
                    replication: {
                        write: {
                            username: 'username',
                            password: 'password'
                        },
                        read: {
                            host: 'host',
                            username: 'username',
                            password: 'password'
                        }
                    }
                }, tunnelConfig);
            });

            should.throw(() => {
                new SequelizeTunnelService({
                    database: 'database',
                    username: 'username',
                    password: 'password',
                    replication: {
                        write: {
                            host: 'host',
                            password: 'password'
                        },
                        read: {
                            host: 'host',
                            username: 'username',
                            password: 'password'
                        }
                    }
                }, tunnelConfig);
            });

            should.not.throw(() => {
                new SequelizeTunnelService({
                    database: 'database',
                    username: 'username',
                    password: 'password',
                    replication: {
                        write: {
                            host: 'host',
                            username: 'username',
                        },
                        read: {
                            host: 'host',
                            username: 'username',
                            password: 'password'
                        }
                    }
                }, tunnelConfig);
            });

            should.not.throw(() => {
                new SequelizeTunnelService({
                    database: 'database',
                    username: 'username',
                    password: 'password',
                    replication: {
                        write: {
                            host: 'host',
                            username: 'username',
                            password: 'password'
                        },
                        read: {
                            host: 'host',
                            username: 'username',
                            password: 'password'
                        }
                    }
                }, tunnelConfig);
            });
        });

        it('should validate tunnelConfig if provided', function() {
            should.not.throw(() => {
                new SequelizeTunnelService(dbConfig, false);
            });

            should.throw(() => {
                new SequelizeTunnelService(dbConfig, {
                    host: 'host',
                    port: 'port',
                    privateKey: 'privateKey'
                });
            });

            should.throw(() => {
                new SequelizeTunnelService(dbConfig, {
                    username: 'username',
                    port: 'port',
                    privateKey: 'privateKey'
                });
            });

            should.throw(() => {
                new SequelizeTunnelService(dbConfig, {
                    username: 'username',
                    host: 'host',
                    privateKey: 'privateKey'
                });
            });

            should.not.throw(() => {
                new SequelizeTunnelService(dbConfig, {
                    username: 'username',
                    host: 'host',
                    port: 'port',
                });
            });

            should.not.throw(() => {
                new SequelizeTunnelService(dbConfig, {
                    username: 'username',
                    host: 'host',
                    port: 'port',
                    privateKey: 'privateKey'
                });
            });
        });
    });

    describe('#establishSequelizeConnection', function() {
        beforeEach(function() {
            SequelizeMock.returns({
                'sequelize': 'sequelize-object'
            });
        });

        it('should create a new database connection and save it', function() {
            sequelizeTunnelService.establishSequelizeConnection('database', 'username', 'password', 'config');

            SequelizeMock.should.have.been.calledOnce.and.calledWithNew;
            SequelizeMock.should.have.been.calledWith('database', 'username', 'password', 'config');

            should.exist(sequelizeTunnelService.db.sequelize);
            sequelizeTunnelService.db.sequelize.should.deep.equal({
                'sequelize': 'sequelize-object'
            });
        });
    });

    describe('#loadModels', function() {
        let sequelizeImportStub;
        let modelAssociateStub;

        beforeEach(function() {
            dbConfig.modelPattern = modelPattern;
            globSyncStub.returns(['file1', 'file2']);

            sequelizeImportStub = sandbox.stub();
            sequelizeTunnelService.db = {
                sequelize: {
                    import: sequelizeImportStub
                }
            };

            modelAssociateStub = sandbox.stub();
            sequelizeImportStub.onCall(0).returns({
                'name': 'file1-model'
            });
            sequelizeImportStub.onCall(1).returns({
                'name': 'file2-model',
                associate: modelAssociateStub
            });
        });

        it('should load all models for the files passed in and set that on the db object', function() {
            sequelizeTunnelService.loadModels();

            globSyncStub.should.have.been.calledWith(modelPattern);

            sequelizeImportStub.should.have.been.calledTwice;
            sequelizeImportStub.firstCall.should.have.been.calledWith('file1');
            sequelizeImportStub.secondCall.should.have.been.calledWith('file2');

            modelAssociateStub.should.have.been.calledOnce.and.calledWith({
                'file1-model': {
                    'name': 'file1-model'
                },
                'file2-model': {
                    'name': 'file2-model',
                    associate: modelAssociateStub
                },
                sequelize: {
                    import: sequelizeImportStub
                }
            });

            sequelizeTunnelService.db.should.deep.equal({
                'file1-model': {
                    'name': 'file1-model'
                },
                'file2-model': {
                    'name': 'file2-model',
                    associate: modelAssociateStub
                },
                sequelize: {
                    import: sequelizeImportStub
                }
            });
        });

        it('should skip loading if no modelPattern was given', function() {
            delete dbConfig.modelPattern;

            sequelizeTunnelService.loadModels();

            globSyncStub.should.not.have.been.called;
        });

        it('should skip loading modelPattern is just spaces', function() {
            dbConfig.modelPattern = '    ';

            sequelizeTunnelService.loadModels();

            globSyncStub.should.not.have.been.called;
        });
    });

    describe('#getConnection', function() {
        let establishSequelizeConnectionStub;
        let loadModelsStub;
        let tunnelOnStub;

        beforeEach(function() {
            establishSequelizeConnectionStub = sandbox.stub(sequelizeTunnelService, 'establishSequelizeConnection');
            loadModelsStub = sandbox.stub(sequelizeTunnelService, 'loadModels');
            tunnelOnStub = sandbox.stub();

            tunnelMock.callsArg(1);
            tunnelMock.returns({
                on: tunnelOnStub
            });
        });

        describe('When db is already connected', function() {
            beforeEach(function() {
                sequelizeTunnelService.db.sequelize = 'exists';
            });

            it('should return the database', function() {
                return sequelizeTunnelService.getConnection().then((db) => {
                    should.exist(db);
                    db.should.deep.equal({
                        Sequelize: SequelizeMock,
                        sequelize: 'exists'
                    });

                    establishSequelizeConnectionStub.should.not.have.been.called;
                    loadModelsStub.should.not.have.been.called;
                });
            });
        });

        describe('when there is no tunnel config', function() {
            beforeEach(function() {
                sequelizeTunnelService = new SequelizeTunnelService(dbConfig, false);
                establishSequelizeConnectionStub = sandbox.stub(sequelizeTunnelService, 'establishSequelizeConnection');
                loadModelsStub = sandbox.stub(sequelizeTunnelService, 'loadModels');

                should.not.exist(sequelizeTunnelService.db.sequelize);
            });

            it('should establish a normal sequelize connection', function() {
                return sequelizeTunnelService.getConnection().then((db) => {
                    should.exist(db);
                    db.should.deep.equal({
                        Sequelize: SequelizeMock
                    });

                    establishSequelizeConnectionStub.should.have.been.calledOnce
                        .and.calledWith(defaultDbConfig.database, defaultDbConfig.username, defaultDbConfig.password, defaultDbConfig);

                    loadModelsStub.should.have.been.calledOnce;
                });
            });
        });

        describe('when there is a tunnel config but no dbConfig replication', function() {
            beforeEach(function() {
                getPortMock.resolves('tunnel-port');
            });

            it('should tunnel a connection and connect sequelize using that connection', function() {
                return sequelizeTunnelService.getConnection().then((db) => {
                    should.exist(db);

                    getPortMock.should.have.been.calledOnce;
                    tunnelMock.should.have.been.calledOnce.and.calledWith({
                        username: tunnelConfig.username,
                        host: tunnelConfig.host,
                        port: tunnelConfig.port,
                        privateKey: tunnelConfig.privateKey,
                        dstHost: defaultDbConfig.host,
                        dstPort: 3306,
                        localHost: '127.0.0.1',
                        localPort: 'tunnel-port',
                        keepAlive: true,
                        keepaliveInterval: 5 * 60 * 1000
                    });

                    establishSequelizeConnectionStub.should.have.been.calledOnce
                        .and.calledWith(defaultDbConfig.database, defaultDbConfig.username, defaultDbConfig.password, {
                            username: dbConfig.username,
                            password: dbConfig.password,
                            database: dbConfig.database,
                            port: 'tunnel-port',
                            host: '127.0.0.1',
                            pool: dbConfig.pool
                        });

                    loadModelsStub.should.have.been.calledOnce;

                    tunnelOnStub.should.have.been.calledTwice;
                });
            });
        });

        describe('when there is a tunnelConfig and dbConfig replication', function() {
            beforeEach(function() {
                dbConfig.replication = {
                    read: {
                        host: 'read-host',
                        username: 'read-username',
                        password: 'read-password'
                    },
                    write: {
                        host: 'write-host',
                        username: 'write-username',
                        password: 'write-password'
                    }

                };
                sequelizeTunnelService = new SequelizeTunnelService(dbConfig, tunnelConfig);
                establishSequelizeConnectionStub = sandbox.stub(sequelizeTunnelService, 'establishSequelizeConnection');
                loadModelsStub = sandbox.stub(sequelizeTunnelService, 'loadModels');

                getPortMock.onCall(0).resolves('tunnel-port');
                getPortMock.onCall(1).resolves('read-tunnel-port');
            });

            it('should tunnel read/write connections and connect sequelize with replication using that connection', function() {
                return sequelizeTunnelService.getConnection().then((db) => {
                    should.exist(db);

                    getPortMock.should.have.been.calledTwice;

                    tunnelMock.should.have.been.calledTwice;
                    tunnelMock.firstCall.should.have.been.calledWith({
                        username: tunnelConfig.username,
                        host: tunnelConfig.host,
                        port: tunnelConfig.port,
                        privateKey: tunnelConfig.privateKey,
                        dstHost: defaultDbConfig.host,
                        dstPort: 3306,
                        localHost: '127.0.0.1',
                        localPort: 'tunnel-port',
                        keepAlive: true,
                        keepaliveInterval: 5 * 60 * 1000
                    });
                    tunnelMock.secondCall.should.have.been.calledWith({
                        username: tunnelConfig.username,
                        host: tunnelConfig.host,
                        port: tunnelConfig.port,
                        privateKey: tunnelConfig.privateKey,
                        dstHost: 'read-host',
                        dstPort: 3306,
                        localHost: '127.0.0.1',
                        localPort: 'read-tunnel-port',
                        keepAlive: true,
                        keepaliveInterval: 5 * 60 * 1000
                    });

                    establishSequelizeConnectionStub.should.have.been.calledOnce
                        .and.calledWith(defaultDbConfig.database, defaultDbConfig.username, defaultDbConfig.password, {
                            database: dbConfig.database,
                            host: dbConfig.host,
                            username: dbConfig.username,
                            password: dbConfig.password,
                            pool: dbConfig.pool,
                            replication: {
                                write: {
                                    username: dbConfig.replication.write.username,
                                    password: dbConfig.replication.write.password,
                                    host: '127.0.0.1',
                                    port: 'tunnel-port'
                                },
                                read: {
                                    username: dbConfig.replication.read.username,
                                    password: dbConfig.replication.read.password,
                                    host: '127.0.0.1',
                                    port: 'read-tunnel-port'
                                }
                            }
                        });

                    loadModelsStub.should.have.been.calledOnce;

                    tunnelOnStub.callCount.should.equal(4);
                });
            });
        });
    });
});
